﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroPong;
namespace pong
{
    public class CommunicationToServer
    {
        public int _player;
        public int _timeStamp;
        public GuiUpdaterInterface _callbacks;

        public CommunicationToServer(GuiUpdaterInterface callbacks)
        {
            _callbacks = callbacks;
        }

        public string sendInfoToServer()
        {
            Info info = new Info(_callbacks.getPlayerPedalLocation(), _timeStamp);
            return serializeJson(info);
        }

        public void dserializeJson(string json)
        {
            State state = JsonConvert.DeserializeObject<State>(json);
            if(state._isMiss)
            {
                _callbacks.addPoint(state._whoMiss);

            }
            if (_player == 1)
                _callbacks.moveOpponentPedal(state._locPlayer2);
            else
                _callbacks.moveOpponentPedal(state._locPlayer1);
            _callbacks.moveBall(state._ballX, state._ballY);
        }

        public string serializeJson(Info info)
        {
            return JsonConvert.SerializeObject(info, Formatting.Indented);
        }
    }
}
