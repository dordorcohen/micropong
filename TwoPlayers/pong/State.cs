﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{

    public class State
    {
        public int _timeStamp;

        public double _locPlayer1;

        public double _locPlayer2;

        public double _ballX;

        public double _ballY;

        public bool _isMiss;

        public bool _whoMiss;

        public int _scorePlayer1;

        public int _scorePlayer2;
    }
}
