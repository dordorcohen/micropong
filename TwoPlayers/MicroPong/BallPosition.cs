﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Worker
{
    public class BallPosition
    {
        public struct Point
        {
            public double x;
            public double y;
        }

        public class Consts
        {
            public const int PLAYER1 = 0;
            public const int PLAYER2 = 1;
            public const int NOT_AT_EDGE = -1;
            public const double BAR_SIZE = 0.3;
            public const double STEP_SIZE = 0.005;
            public const int ANGLE_MAXIMUM = 20; // In degrees
            public const double MIN_X = 0;
            public const double MIN_Y = 0.04;
            public const double MAX_X = 1;
            public const double MAX_Y = 0.96;
        }

        private Point deltaPoint;
        public Point curPoint;
        private Random rnd = new Random(); //Random generator instance for the game
        private int count;
        public BallPosition()
        {
            count = 0;
            this.init();
        }
        public void init() {
            curPoint.x = 0.5;
            curPoint.y = 0.5;
            double ang = rnd.Next(-Consts.ANGLE_MAXIMUM, Consts.ANGLE_MAXIMUM + 1) / 180 * Math.PI;
            if (count %2 == 0)
            {
                ang += Math.PI;
            }
            if (ang == 0) ang = 1;
            deltaPoint.x = Consts.STEP_SIZE * Math.Cos(ang) * 3;
            deltaPoint.y = Consts.STEP_SIZE * Math.Sin(ang) * 3;
            count++;
        }

        public void changeDirection()
        {
            deltaPoint.x *= -1;

        }

        public void getNext()
        {
            curPoint.x += deltaPoint.x;
            curPoint.y += deltaPoint.y;
            if (curPoint.x >= Consts.MAX_X)
            {
                curPoint.x = Consts.MAX_X;
                deltaPoint.x *= -1;
            }
            if (curPoint.x <= Consts.MIN_X)
            {
                curPoint.x = Consts.MIN_X;
                deltaPoint.x *= -1;
            }
            if (curPoint.y >= Consts.MAX_Y)
            {
                curPoint.y = Consts.MAX_Y;
                deltaPoint.y *= -1;
            }
            if (curPoint.y <= Consts.MIN_Y)
            {
                curPoint.y = Consts.MIN_Y;
                deltaPoint.y *= -1;
            }
        }
        public int atPlayerEdge()
        {
            if (curPoint.x >= Consts.MAX_X - 0.1)
            {
                return Consts.PLAYER2;
            }
            else if (curPoint.x <= Consts.MIN_X + 0.1)
            {
                return Consts.PLAYER1;
            }
            else
            {
                return Consts.NOT_AT_EDGE;
            }
        }

        public bool didMiss(double barMid)
        {
            //get the range of the bar:
            double halfBarSize = Consts.BAR_SIZE / 2;
            double barMin = barMid - halfBarSize;
            double barMax = barMid + halfBarSize;
            //check if the x coordinate of the ball is in the bar range:
            if (curPoint.y < barMin || curPoint.y > barMax)
            {
                return true;
            }
            return false;
        }
    }
}
