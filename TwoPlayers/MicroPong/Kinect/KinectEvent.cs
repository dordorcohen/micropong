﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MicroPong.Objects
{
    interface KinectEvent
    {
        void HandMovePlayer(Point point);
        void HandMoveOpponent(Point point);
        void HandPress(bool handOpen);
    }
}
