﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Worker;

namespace MicroPong.Objects
{
    class Kinect
    {
        double playerX =-1 , playerY = -1, opponentX = -1, opponentY = -1;
        private KinectSensor kinectSensor = null;
        private BodyFrameReader bodyFrameReader;
        private CoordinateMapper coordinateMapper;

        private Body[] bodies = null;
        KinectEvent evt;

        ulong playerId = 0;
        ulong oponentId = 0;
        bool gameStarted = false;
        private BallPosition ballPos;

        private GuiUpdaterInterface _guiI;

        public Kinect(KinectEvent evt, GuiUpdaterInterface guiI)
        {
            this.evt = evt;
            _guiI = guiI;
            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // open the sensor
            this.kinectSensor.Open();

            this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;

            ballPos = new BallPosition();
        }

        // Running on frame change
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }
            
            if (dataReceived)
            {
                double x_offset = 200;
                double x_range = 200;

                double y_offset = 100;
                double y_range = 200;

                CameraSpacePoint position;

                IEnumerable<Body> trackedBodies = bodies.Where(b => b.IsTracked);

                Body playerBody = null;
                if (playerId != 0)
                {
                    playerBody = trackedBodies.FirstOrDefault(b => b.TrackingId == playerId);
                }

                if (playerBody == null)
                {
                    playerBody = trackedBodies.FirstOrDefault(b => b.TrackingId != oponentId);
                    playerId = playerBody != null ? playerBody.TrackingId : 0;
                }

                //Body opponentBody = bodies.
                if (playerBody != null)
                {

                    position = playerBody.Joints[JointType.HandRight].Position;
                    DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);

                    var spine = playerBody.Joints[JointType.SpineMid].Position;
                    var x = (position.X - spine.X)*2; // (x_range / 2);
                    var y = (position.Y - spine.Y)*2; // (y_range / 2);


                    //var x = (depthSpacePoint.X - x_offset) / (x_range-50);
                    //var y = (depthSpacePoint.Y - y_offset) / y_range;

                    x = Math.Max(x, 0);
                    x = Math.Min(x, 1);

                    y = Math.Max(y, 0);
                    y = Math.Min(y, 1);
                    playerX = x; playerY = y;

                    this.evt.HandMovePlayer(new Point(x, y));
                    this.evt.HandPress(playerBody.HandRightState == HandState.Closed ? false : true);
                }

                Body opponentBody = null;
                if (oponentId != 0)
                {
                    opponentBody = trackedBodies.FirstOrDefault(b => b.TrackingId == oponentId);
                }

                if (opponentBody == null)
                {
                    opponentBody = trackedBodies.FirstOrDefault(b => b.TrackingId != playerId);
                    oponentId = opponentBody != null ? opponentBody.TrackingId : 0;
                }



                if (opponentBody != null)
                {
                    position = opponentBody.Joints[JointType.HandRight].Position;
                    DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);

                   // var x = (depthSpacePoint.X - x_offset) / x_range;
                    //var y = (depthSpacePoint.Y - y_offset) / y_range;

                    var spine = opponentBody.Joints[JointType.SpineMid].Position;
                    var x = (position.X - spine.X)*2; // (x_range / 2);
                    var y = (position.Y - spine.Y)*2; // (y_range / 2);

                    x = Math.Max(x, 0);
                    x = Math.Min(x, 1);

                    y = Math.Max(y, 0);
                    y = Math.Min(y, 1);
                    opponentX = x;
                    opponentY = y;


                    this.evt.HandMoveOpponent(new Point(x, y));
                    this.evt.HandPress(opponentBody.HandRightState == HandState.Closed ? false : true);
                }

                if (playerBody != null && playerBody.HandRightState == HandState.Closed &&
                    opponentBody != null && opponentBody.HandRightState == HandState.Closed)
                {
                    gameStarted = true;
                }

                if (!gameStarted)
                {
                    return;
                }

                ballPos.getNext();
                //TO DO: get the ball's location
                _guiI.moveBall(ballPos.curPoint.y, ballPos.curPoint.x);
                //if the ball is in one of the players edge, check if it is a miss:
                int playerToCheck = _guiI.ballAtEdge();
                if (playerToCheck != BallPosition.Consts.NOT_AT_EDGE)
                {
                    //now check if there is a miss:
                    double bar = playerToCheck == BallPosition.Consts.PLAYER1 ? opponentX : playerX ;
                    if (ballPos.didMiss(bar))
                    {

                        if (playerToCheck == BallPosition.Consts.PLAYER1)
                        {
                            _guiI.addPoint(true);
                        }
                        else
                        {
                            _guiI.addPoint(false);
                        }
                        ballPos.init();
                        Thread.Sleep(1000);
                    }

                    else
                    {
                        ballPos.changeDirection();
                    }
                }

                _guiI.moveBall(ballPos.curPoint.y, ballPos.curPoint.x);
            }
        }


    }
}

