﻿using MicroPong.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window, KinectEvent
    {
        public Menu()
        {
            InitializeComponent();
            Kinect kinect = new Kinect(this, null);
            
            Loaded += Menu_Loaded;
        }

        private void Menu_Loaded(object sender, RoutedEventArgs e)
        {
            header.Width = screen.Width;
            header.Width = screen.Width;

            Canvas.SetLeft(start_btn, screen.Width/2-start_btn.ActualWidth/2);
        }

        public void HandMovePlayer(Point point)
        {
            //System.Console.WriteLine("(" + point.X + "," + point.Y + "), actualWidth: " + screen.ActualWidth);
            
            Canvas.SetLeft(customMouse, point.X * screen.ActualWidth);
            Canvas.SetTop(customMouse, point.Y * screen.ActualHeight);
        }

        public void HandMoveOpponent(Point point)
        {
            //System.Console.WriteLine("(" + point.X + "," + point.Y + "), actualWidth: " + screen.ActualWidth);

            Canvas.SetLeft(customMouse, point.X * screen.ActualWidth);
            Canvas.SetTop(customMouse, point.Y * screen.ActualHeight);
        }

        public void HandPress(bool handOpen)
        {
            if (handOpen)
                customMouse.Fill = (Brush)(new BrushConverter()).ConvertFrom("#99FFFFFF");
            else
            {
                customMouse.Fill = (Brush)(new BrushConverter()).ConvertFrom("#99FF0000");
                var mouseX = Canvas.GetLeft(customMouse);
                var mouseY = Canvas.GetTop(customMouse);

                var btnLeft = Canvas.GetLeft(start_btn);
                var btnTop = Canvas.GetTop(start_btn);
                var btnRight = Canvas.GetLeft(start_btn)+start_btn.ActualWidth;
                var btnBottom = Canvas.GetTop(start_btn) + start_btn.ActualHeight;

                if (mouseX> btnLeft && mouseX < btnRight && mouseY>btnTop && mouseY < btnBottom)
                {
                    MainWindow game = new MainWindow();
                    game.Show();
                    this.Close();
                }
            }
            // do nothing for now
        }
    }
}
