﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using MicroPong.Objects;
using MicroPong;
using Worker;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, GuiUpdaterInterface, KinectEvent
    {
        /// <summary>
        /// Array for the bodies
        /// </summary>

        private Paddle player;
        private Paddle opponent;
        private Ball ball;
        private Kinect kinect;

        public MainWindow()
        {

            InitializeComponent();

            Loaded += Game_Loaded;

            kinect = new Kinect(this, this);

            //Canvas.SetLeft(player, 25);
        }

        private void Game_Loaded(object sender, RoutedEventArgs e)
        {
            // set screen size
            canvas.Width = screen.ActualHeight * 9 / 12;
            canvas.Height = screen.ActualHeight;

            // initialization
            Util.init(canvas);
            player = new Paddle(xaml_player, Util.serverToCanvas(new Point(BallPosition.Consts.BAR_SIZE, 1)).X);
            opponent = new Paddle(xaml_opponent, Util.serverToCanvas(new Point(BallPosition.Consts.BAR_SIZE, 1)).X);
            ball = new Ball(xaml_ball);

            // place paddles
            opponent.move(new Point(0.5, 0.03));
            player.move(new Point(0.5, 0.97));
            ball.move(new Point(0.5, 0.5));

            // place texts
            Canvas.SetLeft(scoreTitle,Canvas.GetLeft(canvas) + canvas.ActualWidth);
        }

        public void HandMovePlayer(Point point)
        {
            //System.Console.WriteLine("(" + point.X + "," + point.Y + ")");
            Canvas.SetTop(player.getShape(), canvas.ActualHeight - 50);
            Canvas.SetLeft(player.getShape(), point.X * (canvas.ActualWidth - player.getActualWidth()));
        }

        public void HandMoveOpponent(Point point)
        {
            //System.Console.WriteLine("(" + point.X + "," + point.Y + ")");
            //Canvas.SetTop(opponent.getShape(), canvas.ActualHeight - 50);
            Canvas.SetLeft(opponent.getShape(), point.X * (canvas.ActualWidth - opponent.getActualWidth()));
        }

        public void HandPress(bool handOpen)
        {
            // do nothing for now
        }
        
        /// /////////////////////////////////////////////////////////////////////////////////

        public bool moveOpponentPedal(double toXLocation)
        {
            opponent.move(new Point(toXLocation, 0));

            return true;
        }

        public bool moveBall(double toXLocation, double toYLocation)
        {
            ball.move(new Point(toXLocation, toYLocation));

            return true;
        }

        public double getPlayerPedalLocation()
        {
            return player.getLocation().X;
        }

        public bool addPoint(bool playersPoint)
        {
            if (playersPoint)
                playerScore.Text = (int.Parse(playerScore.Text) + 1).ToString();
            else
                opponentScore.Text = (int.Parse(opponentScore.Text) + 1).ToString();
                
            return true;
        }

        public int ballAtEdge()
        {
            if (ball.getLocation().Y <= opponent.getLocation().Y) {
                return 0;
            }
             
            else if( ball.getLocation().Y >= player.getLocation().Y){
                return 1;
            }

            else
                return -1;

        }
    }
}
