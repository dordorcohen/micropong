﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace MicroPong.Objects
{
    class ObjectOnScreen
    {
        protected Shape shape;

        public void move(Point normalizedLocation)
        {
            Point realLocation = Util.serverToCanvas(normalizedLocation);
            var tmp = realLocation.X - shape.Width / 2;
            Canvas.SetLeft(shape, realLocation.X-shape.Width/2);
            tmp = realLocation.Y - shape.Height / 2;
            Canvas.SetTop(shape, realLocation.Y - shape.Height / 2);
        }

        public Point getLocation()
        {
            //(new List<string>()).Select(str => str[0]).Where(chr => chr > 'A').GroupBy(chr => chr).OrderBy(grp => grp.Count)
            return Util.canvasToServer(new Point(Canvas.GetLeft(shape),Canvas.GetTop(shape)));
        }
    }
}
