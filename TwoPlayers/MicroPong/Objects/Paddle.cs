﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace MicroPong.Objects
{
    class Paddle : ObjectOnScreen
    {
        public Paddle(Rectangle paddle, double width)
        {
            this.shape = paddle;
            paddle.Width = width;
        }

        public double getActualWidth()
        {
            return shape.ActualWidth;
        }

        public Shape getShape()
        {
            return shape;
        }

    }
}
