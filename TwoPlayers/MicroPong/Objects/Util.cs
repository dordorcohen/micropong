﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MicroPong.Objects
{
    static class Util
    {
        
        public static double canvasWidth, canvasHeight;

        public static void init(Canvas canvas)
        {
            canvasWidth = canvas.Width;
            canvasHeight = canvas.Height;
        }

        public static Point serverToCanvas(Point location)
        {
            return new Point(location.X * canvasWidth, location.Y * canvasHeight);
        }

        public static Point canvasToServer(Point location)
        {
            return new Point(location.X / canvasWidth, location.Y / canvasHeight);
        }
    }
}
