﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroPong
{
    public class Info
    {

        public int _timeStamp;

        public double _location;

        public int _id;

        public Info(double loc, int time , int id)
        {
            _location = loc;
            _timeStamp = time;
            _id = id;
        }
    }
}
