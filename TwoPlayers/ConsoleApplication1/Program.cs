﻿using MicroPong;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program : GuiUpdaterInterface
    {
        private static UTF8Encoding encoding = new UTF8Encoding();

        private static string url = "http://summerpong.cloudapp.net/api/game";

        // private WebRequest request;

        //private Stream dataStream;

        private string status;

        private static string id;

        private static HttpClient client;

        static void Main(string[] args)
        {
            //Connect("ws://10.85.162.24/WebsocketsSample").Wait();
            Connect(new Program()).Wait();
            //Connect("ws://localhost:99/WebsocketsSample",new Program()).Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        public static async Task Connect(GuiUpdaterInterface inter)
        {
            Thread.Sleep(1000); //wait for a sec, so server starts and ready to accept connection..
            int _id;
            client = new HttpClient();

            //get player's id
            var result = await client.GetAsync(new Uri(url));
            id = result.Content.ToString();
            if (result.StatusCode != HttpStatusCode.OK)
            {
                id = "0";
                Console.WriteLine("WARNING: player's id was not recieved from server, using id 0");
            }
            else if (!int.TryParse(id, out _id)){
                id = "0";
                Console.WriteLine("WARNING: player's id from server is not a valid integer, using id 0");
            }

            try
            {

                await Task.WhenAll(Receive(inter), Send(inter));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex);
            }

        }

        private static async Task Send(GuiUpdaterInterface inter)
        {
            CommunicationToServer cts = new CommunicationToServer(inter, int.Parse(id));
            while (true)
            {
                string json = cts.sendInfoToServer();

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "text/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }

                await Task.Delay(1000);
            }
        }

        private static async Task Receive(GuiUpdaterInterface inter)
        {
            while (true)
            {
                var result = await client.GetAsync(new Uri(url + "/" + id));
                string json = result.Content.ToString();
                if (result.StatusCode != HttpStatusCode.OK)
                {
                    Console.WriteLine("ERROR: could not recieve game information from server!");
                }

                else
                {
                    CommunicationToServer cts = new CommunicationToServer(inter, int.Parse(id));
                    cts.dserializeJson(json);
                }
            }
        }

        public bool moveOpponentPedal(double toXLocation)
        {
            throw new NotImplementedException();
        }

        public bool moveBall(double toXLocation, double toYLocation)
        {
            throw new NotImplementedException();
        }

        public double getPlayerPedalLocation()
        {
            throw new NotImplementedException();
        }

        public bool addPoint(bool playersPoint)
        {
            throw new NotImplementedException();
        }

        public int ballAtEdge()
        {
            throw new NotImplementedException();
        }
    }
}
