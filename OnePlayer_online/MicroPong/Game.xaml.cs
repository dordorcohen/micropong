﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using MicroPong.Objects;
using MicroPong;
using System.Timers;
using System.Windows.Threading;
using System.Diagnostics;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, GuiUpdaterInterface, KinectEvent
    {
        /// <summary>
        /// Array for the bodies
        /// </summary>

        private Paddle player;
        private Paddle opponent;
        private Ball ball;
        private bool movePaddle;
        private Stopwatch lastPaddleDropped;

        private Kinect kinect;
        CommunicationToServer comm;

        public MainWindow()
        {

            InitializeComponent();

            Loaded += Game_Loaded;

            kinect = new Kinect(this);

            //Canvas.SetLeft(player, 25);
        }

        private void Game_Loaded(object sender, RoutedEventArgs e)
        {
            // set screen size
            canvas.Width = screen.ActualHeight * 9 / 12;
            canvas.Height = screen.ActualHeight;

            // initialization
            Util.init(canvas);
            player = new Paddle(xaml_player, Util.serverToCanvas(new Point(0.2,0.04)));
            xaml_player.Fill = new SolidColorBrush(Colors.Red);
            opponent = new Paddle(xaml_opponent, Util.serverToCanvas(new Point(0.2, 0.04)));
            ball = new Ball(xaml_ball);

            // place paddles
            opponent.move(new Point(0.5, 0.03));
            player.move(new Point(0.5, 0.97));
            ball.move(new Point(0.5, 0.5));

            // place texts
            Canvas.SetLeft(scoreTitle,Canvas.GetLeft(canvas) + canvas.ActualWidth);

            // Communication
            comm = new CommunicationToServer(this);
            comm.connect();
            //comm.recieveFromServer();

            Timer timer = new Timer(200); //20 secs for testing purpose.
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += OnTimerTick;
            timer.Start();
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            comm.recieveFromServer();
            comm.sendInfoToServer();
        }


        public void HandMove(Point point)
        {
            //System.Console.WriteLine("(" + point.X + "," + point.Y + ")");
            xaml_player.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF00AEFF"));
            //if (movePaddle)
            //{
                Canvas.SetTop(player.getShape(), canvas.ActualHeight - 50);
                Canvas.SetLeft(player.getShape(), point.X * (canvas.ActualWidth - player.getActualWidth()));
            //}
        }

        public void HandPress(bool handOpen)
        {
            return;
            if (handOpen)
            {
                xaml_player.Fill = new SolidColorBrush(Colors.Red);
            }
            else
            {
                xaml_player.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF00AEFF"));
            }

            this.movePaddle = !handOpen;
        }
        
        /// /////////////////////////////////////////////////////////////////////////////////

        public bool moveOpponentPedal(double toXLocation)
        {
            this.Dispatcher.Invoke(() => opponent.move(new Point(toXLocation, 0.03)));

            return true;
        }

        public bool moveBall(double toXLocation, double toYLocation)
        {
            this.Dispatcher.Invoke(() => ball.move(new Point(toYLocation, toXLocation)));

            return true;
        }

        public double getPlayerPedalLocation()
        {
            double x = 0;

            this.Dispatcher.Invoke(() => x = player.getLocation().X);

            return x;
        }

        public bool setPoints(int scorePlayer1, int scorePlayer2)
        {
            this.Dispatcher.Invoke(() =>
            {
                playerScore.Text = scorePlayer1.ToString();
                opponentScore.Text = scorePlayer2.ToString();
            });
                
            return true;
        }
    }
}
