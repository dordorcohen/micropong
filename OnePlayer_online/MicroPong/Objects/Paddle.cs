﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace MicroPong.Objects
{
    class Paddle : ObjectOnScreen
    {
        public Paddle(Rectangle paddle, Point dim)
        {
            this.shape = paddle;
            paddle.Width = dim.X;
            paddle.Height = dim.Y;
        }

        public double getActualWidth()
        {
            return shape.ActualWidth;
        }

        public Shape getShape()
        {
            return shape;
        }

    }
}
