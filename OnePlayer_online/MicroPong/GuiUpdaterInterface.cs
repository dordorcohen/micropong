﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroPong
{
    public interface GuiUpdaterInterface
    {
        /**
        *param name="toXLocation"
        *Move the opponent's pedal to the input location
        */
        bool moveOpponentPedal(double toXLocation);

        /**
        *param name="toXLocation"
        *param name="toYLocation"
        *Move the ball to the input location coordinates
        */
        bool moveBall(double toXLocation, double toYLocation);

        /**
        *return's the x coordinate of the player's pedal
        */
        double getPlayerPedalLocation();

        /**
        *param name="playersPoint"
        *if playersPoint is true, then the player scored a point,
        *else - the opponent scored a point
        *
        *The score board will be updated
        */
        bool setPoints(int scorePlayer1, int scorePlayer2);
    }
}
