﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace MicroPong.Objects
{
    class Kinect
    {

        private KinectSensor kinectSensor = null;
        private BodyFrameReader bodyFrameReader;
        private CoordinateMapper coordinateMapper;

        private Body[] bodies = null;
        KinectEvent evt;

        ulong playerId = 0;

        public Kinect(KinectEvent evt)
        {
            this.evt = evt;

            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // open the sensor
            this.kinectSensor.Open();

            this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
        }

        // Running on frame change
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                double x_offset = 200;
                double x_range = 200;

                double y_offset = 100;
                double y_range = 200;

                CameraSpacePoint position;

                Body playerBody = null;
                if (playerId != 0)
                {
                    playerBody = bodies.FirstOrDefault(b => b.TrackingId == playerId);
                }

                if (playerBody == null)
                {
                    playerBody = bodies.FirstOrDefault(b => b.IsTracked);
                    playerId = playerBody != null ? playerBody.TrackingId : 0;
                }

                if (playerBody != null)
                {

                    position = playerBody.Joints[JointType.HandRight].Position;
                    DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);

                    var x = (depthSpacePoint.X - x_offset) / x_range;
                    var y = (depthSpacePoint.Y - y_offset) / y_range;

                    x = Math.Max(x, 0);
                    x = Math.Min(x, 1);

                    y = Math.Max(y, 0);
                    y = Math.Min(y, 1);

                    this.evt.HandMove(new Point(x, y));
                    this.evt.HandPress(playerBody.HandRightState == HandState.Closed ? false : true);
                }
            }
        }


    }
}

