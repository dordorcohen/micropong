﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroPong
{
    public class CommunicationToServer
    {
        private static string url = "http://summerpong.cloudapp.net/api/game";
        public int _player;
        public int _timeStamp;
        public GuiUpdaterInterface _callbacks;

        public CommunicationToServer(GuiUpdaterInterface callbacks)
        {
            _callbacks = callbacks;
        }

        public void connect()
        {
            using (var webClient = CreateClient())
            {
                webClient.Headers.Add("ContentType", "application/json");
                _player = int.Parse(webClient.DownloadString(url));
                _player = 0;
            }
        }

        public void sendInfoToServer()
        {
            using (var webClient = CreateClient())
            {
                Info info = new Info(_callbacks.getPlayerPedalLocation(), _timeStamp);
                string data = serializeJson(info);
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(data);
                Uri uri = new Uri(url + "/" + _player);
                webClient.UploadData(uri, "POST", bytes);
                //string result = HttpPost(url + "/" + _player, data);
            }
        }
        

        public void recieveFromServer()
        {
            string json;
            using (var webClient = CreateClient())
            {
                json = webClient.DownloadString(url + "/" + _player).Replace("\\\"", "\"");
            }

            json = json.Substring(1, json.Length - 2);
            
            State state = JsonConvert.DeserializeObject<State>(json);
            _callbacks.setPoints(state._scorePlayer1, state._scorePlayer2);
            if (_player == 0)
                _callbacks.moveOpponentPedal(state._locPlayer2);
            else // _player == 1
                _callbacks.moveOpponentPedal(state._locPlayer1);
            _callbacks.moveBall(state._ballX, state._ballY);
            _timeStamp = state._timeStamp;
        }

        private string serializeJson(Info info)
        {
            return JsonConvert.SerializeObject(info, Formatting.Indented);
        }

        private static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            //Add these, as we're doing a POST
            req.ContentType = "application/json ";
            req.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

        private static WebClient CreateClient()
        {
            var webClient = new WebClient();
            webClient.Headers.Add("Content-Type", "application/json");

            return webClient;
        }
    }
}
