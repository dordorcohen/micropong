﻿using MicroPong;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Communication
    {
        string user_id = "";

        public void Connect()
        {
            WebClient webClient = new WebClient();
            /*webClient.QueryString.Add("param1", "value1");
            webClient.QueryString.Add("param2", "value2");*/
            user_id = webClient.DownloadString("http://summerpong.cloudapp.net/api/game");
        }

        public State getUpdate()
        {
            WebClient webClient = new WebClient();
            /*webClient.QueryString.Add("param1", "value1");
            webClient.QueryString.Add("param2", "value2");*/
            string json = webClient.DownloadString("http://summerpong.cloudapp.net/api/game/" + user_id).Replace("\\\"","\"");
            json = json.Substring(1, json.Length - 2);

            var state = new State();
            state = JsonConvert.DeserializeObject<State>(json);
            return state;
        }

        public static string HttpPost(string URI, string Parameters)
        {
            System.Net.WebRequest req = System.Net.WebRequest.Create(URI);
            //Add these, as we're doing a POST
            req.ContentType = "application/x-www-form-urlencoded";
            req.Method = "POST";
            //We need to count how many bytes we're sending. Post'ed Faked Forms should be name=value&
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(Parameters);
            req.ContentLength = bytes.Length;
            System.IO.Stream os = req.GetRequestStream();
            os.Write(bytes, 0, bytes.Length); //Push it out there
            os.Close();
            System.Net.WebResponse resp = req.GetResponse();
            if (resp == null) return null;
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }

    }
}
